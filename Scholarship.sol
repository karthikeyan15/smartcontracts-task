pragma solidity ^0.6.0;

contract Scholarship {
    address payable public classes_7;
    address payable public student_1 = 0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2;
    constructor() public {
        classes_7 = msg.sender;
    }
    mapping(uint => Pay_detail) public Transactions; 
    struct Pay_detail {
        address from;
        address to;
        address to_whom;
        uint amount;
    }
    Pay_detail public pd;
    function fund_transfer() payable external{
        classes_7.transfer(msg.value);
        pd = Pay_detail(msg.sender, classes_7, student_1, msg.value);
    }
}
